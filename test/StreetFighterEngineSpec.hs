module StreetFighterEngineSpec where

import qualified Data.Set as Set

import qualified Data.Sequence as Seq
import HardCodedParameters as D


import Model
import Test.QuickCheck
import Test.Hspec

-- check individual Pegs

genPair :: Gen a -> Gen b -> Gen (a, b)
genPair genFst genSnd = do
  x <- genFst
  y <- genSnd
  return (x,y)

exCoord = Coord 100 100

exHitbox = Rect (Coord 400 220) 30 30

exMvOk = Mouv D 10

checkMovements= do
  describe "Movements" $ do
    it "returns same coordonates (Coord 110 110)" $ do
      bougeCoord exCoord (Mouv D 10)
        `shouldBe` Coord 110 100

checkHitbox= do
  describe "Hitbox" $ do
    it "returns always true (invariant hitbox sur  Rect (Coord 400 220) 30 30)" $ do
      propInvHitbox exHitbox
        `shouldBe` True

jeuSpecInit = do
  describe "BougeJeu" $ do
    it "Preserves the invariant après chaque mouvement aléatoire" $ property property_inv_bougeJoueur

checkDamage = do
  describe "Damage" $ do
    it "returns same EtatCombattant (Ok 50)" $ do
      getEtatCombattant (damagePlayer initJeu Player1 50) Player1
        `shouldBe` (Ok 50)

checkDamageKick = do
  describe "Damage" $ do
    it "returns same EtatCombattant (Ok 100-kickDamage)" $ do
      getEtatCombattant (effectKick initJeu Player1) Player2
        `shouldBe` (Ok (100-D.kickDamage))


jeuSpecHitBox = do
  describe "HitBox correct" $ do
    it "Preserves the invariant pour chaque hitbox généré" $ property property_inv_hitbox

jeuSpecHitBoxHit = do
  describe "Hitbox hit" $ do
    it "Always in collision between the thow Hitbox" $ property property_hitbox_hit

property_inv_hitbox :: Property
property_inv_hitbox = withMaxSuccess 1000 (forAll genHitboxZone $ uncurry verifHitbox)

property_hitbox_hit :: Property
property_hitbox_hit = withMaxSuccess 1000 (forAll genDeuxHitboxHit $ uncurry collision)

property_inv_bougeJoueur :: Property
property_inv_bougeJoueur = withMaxSuccess 1000 (forAll (genPair genJeuOk genMouvement) $ uncurry propBougeJoueur)



-- Exemple de générateur garantissant l'invariant
genHitboxZone :: Gen (Hitbox, Zone)
genHitboxZone = do
  zoneJeu <- choose (800,1600)
  x <- choose (0, 200)  -- x de la hitbox
  y <- choose (0, 200) -- y de la hitbox
  largHitbox <- choose (0, 400) -- taille de la Hitbox 
  longHitbox <- choose (0, 400)
  let hitb = Rect (Coord x y) largHitbox longHitbox
  return (hitb,Zone zoneJeu zoneJeu)


genDeuxHitboxHit :: Gen (Hitbox, Hitbox)
genDeuxHitboxHit = do
  x1 <- choose (0, 200)  -- x de la hitbox
  y1 <- choose (0, 200) -- y de la hitbox
  x2 <- choose (0, 200)  -- x de la hitbox
  y2 <- choose (0, 200) -- y de la hitbox
  largHitbox1 <- choose (250, 400) -- taille de la Hitbox 
  longHitbox1 <- choose (250, 400)
  largHitbox2 <- choose (250, 400) -- taille de la Hitbox 
  longHitbox2 <- choose (250, 400)
  let hitb1 = Rect (Coord x1 y1) largHitbox1 longHitbox1
  let hitb2 = Rect (Coord x2 y2) largHitbox2 longHitbox2
  return (hitb1, hitb2)


genJeuOk :: Gen Jeu
genJeuOk = do
    yPlayers <- choose (200, 400)  -- Hauteurs des personnages
    x1 <- choose (100, 200)  -- x du j1
    x2 <- choose (250, 400) -- x du j2
    largHitbox <- choose (0, 100) -- taille de la Hitbox 
    longHitbox <- choose (0, 100)
    sante <- choose (1,100) -- sante > 0
    zoneJeu <- choose (500,600)
    return $ mkJeu x1 x2 yPlayers largHitbox longHitbox sante zoneJeu


genMouvement :: Gen Mouvement
genMouvement = do
  d <- elements [H,B,D,G]
  val <- choose (0,1000)
  return $ Mouv d val


engineSpec = do
  checkMovements
  checkHitbox
  checkDamage
  checkDamageKick
  jeuSpecInit
  jeuSpecHitBoxHit
  jeuSpecHitBox
  




