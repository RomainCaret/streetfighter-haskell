module SplitSpec where

import Test.Hspec
import Test.QuickCheck

import Split

splitSpec0 = do
  describe "split" $ do
    it "splits a string wrt. a given character into a list of words" $ --entre chaque '/' sépare les bouts
      (split '/' "aa/bb/ccc/dd d") `shouldBe` ["aa", "bb", "ccc", "dd d"] --test unitaire sur un exemple simple

splitSpec1 = do
  describe "split" $ do
    it "can be undone with unsplit (v1)" $ property $  --On utilise property pour afficher la distribution
      \c xs -> collect (length xs) $ prop_split_unsplit c xs --des tailles des tests

splitSpec2 = do
  describe "split" $ do
    it "can be undone with unsplit (v2)" $ property $ --On utilise property pour afficher la distribution des tailles des textes
      \xs -> forAll (elements xs) $ \c -> collect (length xs) $ prop_split_unsplit c xs --forAll wrap (elements qui choisit un élement à tester parmis une liste) pour pouvoir mettre une propriété dessus et le tester

-- Remarque : on utilise comme caractère de split les éléments des listes `xs` en entrée,
--            cf. la doc QuickCheck sur `forAll`, `elements`, etc.


splitSpec3 = do
  describe "split" $ do
    it "can be undone with unsplit (v3)" $ property $
      forAll (oneof [return "bla bla bli"
                     , return "toto"
                     , return ""
                     , return "un    deux trois   quatre"]) $
      \xs -> prop_split_unsplit ' ' xs


--CODE MASTERMIND TEST
--markCorrectOneSpec = do
 -- describe "markCorrectOne" $ do

  --  it "returns a mark 'correct' if the guess and secret pegs are equal" $ do
   --   markCorrectOne Yellow (Yellow, Unmarked) `shouldBe` (PEmpty, (Yellow, MarkedCorrect))

  --  it "returns an unchanged mark if the guess and secret pegs are inequal" $ do
    --  markCorrectOne Blue (Yellow, Unmarked) `shouldBe` (Blue, (Yellow, Unmarked)) 

--markCorrectSpec = do
  --describe "markCorrect" $ do

    --it "returns no correct mark if the whole guess does not correspond to the secret" $ do
    --  let guess = Seq.fromList [Blue, Blue, Blue, Red]
    --      feedback = initFeedback exSecret
     --     res = markCorrect guess feedback
      --  in res `shouldBe` (guess, feedback)

   -- it "returns a single correct mark and empty the corresponding guess" $ do
    --  let guess = Seq.fromList [Blue, Blue, Green, Red]
      --    feedback = initFeedback exSecret
    --      res = markCorrect guess feedback
     --   in res `shouldBe` (Seq.fromList [Blue, Blue, PEmpty, Red]
      --                    ,Seq.fromList [(Yellow, Unmarked), (Yellow, Unmarked), (Green, MarkedCorrect), (Blue, Unmarked)])

   -- it "returns a two correct marks and empty the corresponding guesses" $ do
     -- let guess = Seq.fromList [Yellow, Blue, Green, Red]
      --    feedback = initFeedback exSecret
      --    res = markCorrect guess feedback
    --    in res `shouldBe` (Seq.fromList [PEmpty, Blue, PEmpty, Red]
           --               ,Seq.fromList [(Yellow, MarkedCorrect), (Yellow, Unmarked), (Green, MarkedCorrect), (Blue, Unmarked)])

    --it "returns all correct marks and empty all guesses" $ do
     -- let guess = Seq.fromList [Yellow, Yellow, Green, Blue]
      --    feedback = initFeedback exSecret
       --   res = markCorrect guess feedback
     --   in res `shouldBe` (Seq.fromList [PEmpty, PEmpty, PEmpty, PEmpty]
      --                    ,Seq.fromList [(Yellow, MarkedCorrect), (Yellow, MarkedCorrect), (Green, MarkedCorrect), (Blue, MarkedCorrect)])
           

--markPositionSpec = do
 -- describe "markPosition" $ do
--
  --  it "returns a single position mark for first guess" $ do
 --     markPosition (Seq.fromList [Blue, Red, Red, Red])
  --                 (Seq.fromList [(Yellow, Unmarked), (Yellow, Unmarked), (Green, Unmarked), (Blue, Unmarked)])
     --   `shouldBe` (Seq.fromList [(Yellow, Unmarked), (Yellow, Unmarked), (Green, Unmarked), (Blue, MarkedPosition)])

  --  it "does not mark the position if it is already correct" $ do
   --   markPosition (Seq.fromList [Blue, Red, Red, Blue])
  --                 (Seq.fromList [(Yellow, Unmarked), (Yellow, Unmarked), (Green, Unmarked), (Blue, MarkedCorrect)])
   --     `shouldBe` (Seq.fromList [(Yellow, Unmarked), (Yellow, Unmarked), (Green, Unmarked), (Blue, MarkedCorrect)])

  --  it "skips empty guesses and returns a single position mark for third guess" $ do
   --   markPosition (Seq.fromList [PEmpty, PEmpty, Blue, Red])
   --                (Seq.fromList [(Yellow, Unmarked), (Yellow, Unmarked), (Green, Unmarked), (Blue, Unmarked)])
 --       `shouldBe` (Seq.fromList [(Yellow, Unmarked), (Yellow, Unmarked), (Green, Unmarked), (Blue, MarkedPosition)])

   -- it "favors the leftmost match" $ do
  --    markPosition (Seq.fromList [PEmpty, Red, Red, Yellow])
  --                 (Seq.fromList [(Yellow, Unmarked), (Yellow, Unmarked), (Green, Unmarked), (Blue, Unmarked)])
   --     `shouldBe` (Seq.fromList [(Yellow, MarkedPosition), (Yellow, Unmarked), (Green, Unmarked), (Blue, Unmarked)])

 --   it "matches multiple times if needed" $ do
  --    markPosition (Seq.fromList [PEmpty, Red, Yellow, Yellow])
  --                 (Seq.fromList [(Yellow, Unmarked), (Yellow, Unmarked), (Green, Unmarked), (Blue, Unmarked)])
  --      `shouldBe` (Seq.fromList [(Yellow, MarkedPosition), (Yellow, MarkedPosition), (Green, Unmarked), (Blue, Unmarked)])

 --   it "it also works with multiple matches" $ do
  --    markPosition (Seq.fromList [PEmpty, Green, Blue, Yellow])
  --                 (Seq.fromList [(Yellow, Unmarked), (Yellow, Unmarked), (Green, Unmarked), (Blue, Unmarked)])
 --       `shouldBe` (Seq.fromList [(Yellow, MarkedPosition), (Yellow, Unmarked), (Green, MarkedPosition), (Blue, MarkedPosition)])

-- tests à compléter > OK
--verifySpec = do
 -- describe "verify" $ do
--    it "correctly answers with 0 correct peg and 0 position" $ do
--      verify exSecret (Seq.fromList [Black, Cyan, White, Magenta]) `shouldBe` (Answer 0 0)
  --  it "correctly answers with 1 correct peg and 0 position" $ do
   --   verify exSecret (Seq.fromList [Yellow, Cyan, White, Magenta]) `shouldBe` (Answer 1 0)
   -- it "correctly answers with 1 correct peg and 1 position" $ do
   --   verify exSecret (Seq.fromList [Blue, Yellow, White, Magenta]) `shouldBe` (Answer 1 1)
   -- it "correctly answers with 2 correct peg and 1 position" $ do
   --   verify exSecret (Seq.fromList [Blue, Yellow, Green, Magenta]) `shouldBe` (Answer 2 1)
   -- it "correctly answers with 2 correct peg and 2 position" $ do
     -- verify exSecret (Seq.fromList [Yellow, Yellow, Blue, Green]) `shouldBe` (Answer 2 2)
   -- it "correctly answers with 0 correct peg and 4 positions" $ do
     -- verify exSecret (Seq.fromList [Green, Blue, Yellow, Yellow]) `shouldBe` (Answer 0 4)
    --it "correctly answers with 4 correct peg and 0 positions" $ do
    --  verify exSecret (Seq.fromList [Yellow, Yellow, Green, Blue]) `shouldBe` (Answer 4 0)

--winningSpec = do
 -- describe "winning" $ do
  --  it "says that there's a win in the correct situation" $ do
  --    winning exSecret  (verify exSecret (Seq.fromList [Yellow, Yellow, Green, Blue])) `shouldBe` True
  --  it "says False otherwise" $ do
   --   winning exSecret  (verify exSecret (Seq.fromList [Yellow, Yellow, Green, Red])) `shouldBe` False
      
