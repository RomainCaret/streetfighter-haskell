module HardCodedParameters where


sizeStep :: Int
sizeStep = 3

startupFramesGarde :: Integer 
startupFramesGarde = 10

recoveryFramesGarde :: Integer 
recoveryFramesGarde = 10



walkFrameNumber :: Integer
walkFrameNumber = 3

jumpFrameNumber :: Integer
jumpFrameNumber = 3

kickFrameNumber :: Integer
kickFrameNumber = 2

blockFrameNumber :: Integer
blockFrameNumber = 1

punchFrameNumber :: Integer
punchFrameNumber = 3

koFrameNumber :: Integer
koFrameNumber = 3


walkSpriteDuration :: Integer
walkSpriteDuration = 5

jumpSpriteDuration :: Integer
jumpSpriteDuration = 4

kickSpriteDuration :: Integer
kickSpriteDuration = 5

punchSpriteDuration :: Integer
punchSpriteDuration = 5

koSpriteDuration :: Integer
koSpriteDuration = 5


idleSpriteBegin :: Integer
idleSpriteBegin = 0

walkSpriteBegin :: Integer
walkSpriteBegin = 0

jumpSpriteBegin :: Integer
jumpSpriteBegin = 4

kickSpriteBegin :: Integer
kickSpriteBegin = 6

blockSpriteBegin :: Integer
blockSpriteBegin = 8

crouchSpriteBegin :: Integer
crouchSpriteBegin = 9

punchSpriteBegin :: Integer
punchSpriteBegin = 10

koSpriteBegin :: Integer
koSpriteBegin = 15

punchDamage :: Integer
punchDamage = 10

kickDamage :: Integer
kickDamage = 10

jumpSpeed :: Integer
jumpSpeed = 5

