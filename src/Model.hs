module Model where

import SDL
import Keyboard (Keyboard)
import qualified Keyboard as K
import Screen as S
import HardCodedParameters as D
import Data.Sequence (Seq (..))
import Data.Foldable


--Data Mouvements (Partie 1)
data Zone = Zone Integer Integer deriving(Show)
data Coord = Coord Integer Integer deriving(Show)
data Direction = H | B | G | D deriving(Show, Eq)
data NumPlayer = Player1 | Player2
data Mouvement = Mouv Direction Integer deriving(Show)



--Data Hitbox (Partie 2)
data Hitbox = Rect Coord Integer Integer
  | Composite (Seq Hitbox) deriving(Show)

--Data Jeu (Partie 3)
data EtatCombattant = Ko
  | Ok Integer deriving(Show,Eq) -- sa sante actuelle

data Combattant = Comb {
  positionc :: Coord,
  hitboxc :: Hitbox,
  facec :: Direction,
  etatx :: EtatCombattant,
  action :: Action
} deriving(Show)

data Jeu = GameOver Integer -- numéro du vainqueur
  | EnCours {
    joueur1 :: Combattant,
    joueur2 :: Combattant,
    zoneJeu :: Zone
  } deriving(Show)

--instances :
instance Eq Coord where
  (==) = coordEqu

coordEqu :: Coord -> Coord -> Bool
coordEqu (Coord x1 y1) (Coord x2 y2) = x1 == x2 && y1 == y2




--Initialisation du jeu (Etat du jeu au début)
initJeu :: Jeu
initJeu = EnCours {joueur1 = Comb{positionc = Coord 400 220,
                                      hitboxc = Rect (Coord 400 220) 30 75,
                                      facec = D,
                                      etatx = Ok 100,
                                      action = Action Idle 0},
                      joueur2 = Comb{positionc = Coord 100 220,
                                      hitboxc = Rect (Coord 100 220) 30 75,
                                      facec = G,
                                      etatx = Ok 100,
                                      action = Action Idle 0},
                      zoneJeu = Zone (fromIntegral S.screenW) (fromIntegral S.screenH)}

--mkJeu avec paramètres (Pour les tests)
mkJeu :: Integer -> Integer -> Integer -> Integer -> Integer -> Integer -> Integer -> Jeu
mkJeu x1 x2 yPlayers largHitbox longHitbox sante zoneJeu =
  EnCours {joueur1 = Comb{positionc = Coord x1 yPlayers,
                                      hitboxc = Rect (Coord x1 yPlayers) largHitbox longHitbox,
                                      facec = D,
                                      etatx = Ok sante,
                                      action = Action Idle 0},
                      joueur2 = Comb{positionc = Coord x2 yPlayers,
                                      hitboxc = Rect (Coord x2 yPlayers) largHitbox longHitbox,
                                      facec = G,
                                      etatx = Ok sante,
                                      action = Action Idle 0},
                      zoneJeu = Zone zoneJeu zoneJeu}

--bougeCoord sans vérification
bougeCoord :: Coord -> Mouvement -> Coord
bougeCoord (Coord x y) (Mouv d nb) = case d of
  H -> Coord x (y-nb)
  B -> Coord x (y+nb)
  G -> Coord (x-nb) y
  D -> Coord (x+nb) y

--bougeCoord avec vérification de la zone du jeu
bougeCoordSafe :: Coord -> Mouvement -> Zone -> Maybe Coord
bougeCoordSafe c m = verifCoord (bougeCoord c m)

--verification si les coordonées sont toujours dans la zone
verifCoord :: Coord -> Zone -> Maybe Coord
verifCoord (Coord x y) (Zone w h)
    | x < 0 || x > w || y < 0 || y > h = Nothing
    | otherwise = Just (Coord x y)

--Permet de se libérer de Maybe
bougeCoordSafeDefault :: Coord -> Mouvement -> Zone -> Coord
bougeCoordSafeDefault startC mv z = case bougeCoordSafe startC mv z of
  Nothing -> startC
  Just finishC -> finishC

--Propriété à vérifier (Gauche Droite = Identité)
prop_gaucheDroite_bougeCoord :: Coord -> Integer -> Bool
prop_gaucheDroite_bougeCoord c nb = bougeCoord (bougeCoord c (Mouv G nb)) (Mouv D nb) == c

-- Récupération de x et y selon Coord et des Coord des joueurs et des élements nécéssaires pour l'affichage (Getters)
getX :: Coord -> Integer
getX (Coord x _) = x

getY :: Coord -> Integer
getY (Coord _ y) = y

getCoord :: Jeu -> NumPlayer -> Coord
getCoord (GameOver c) _ = Coord 400 220
getCoord j Player1 = positionc (joueur1 j)
getCoord j Player2 = positionc (joueur2 j)

getCoordJ1 :: Jeu -> Coord
getCoordJ1 (GameOver c) = Coord 400 220
getCoordJ1 j = positionc (joueur1 j)

getCoordJ2 :: Jeu -> Coord
getCoordJ2 (GameOver c) = Coord 100 220
getCoordJ2 j = positionc (joueur2 j)

getDir :: Jeu -> NumPlayer -> Direction
getDir (GameOver c) _ = D
getDir j Player1 = facec (joueur1 j)
getDir j Player2 = facec (joueur2 j)

getEtatsCombattants :: Jeu -> (EtatCombattant,EtatCombattant)
getEtatsCombattants (GameOver c) = (Ko,Ko)
getEtatsCombattants j = (etatx (joueur1 j),etatx (joueur2 j))

getEtatCombattant :: Jeu -> NumPlayer -> EtatCombattant
getEtatCombattant (GameOver c) _ = Ko
getEtatCombattant j  Player1 = etatx (joueur1 j)
getEtatCombattant j  Player2 = etatx (joueur2 j)

getAction :: Jeu -> NumPlayer -> Action
getAction (GameOver c) _ = Action Idle 0
getAction j Player1 = action (joueur1 j)
getAction j Player2 = action (joueur2 j)

getFrame :: Jeu -> NumPlayer -> Integer
getFrame j pl = case getAction j pl of
  Action _ val -> val

setDir :: Jeu -> NumPlayer -> Direction -> Jeu
setDir j@EnCours {joueur1 = j1} Player1 dir = j {joueur1 = j1 {facec = dir}}
setDir j@EnCours {joueur2 = j2} Player2 dir = j {joueur2 = j2 {facec = dir}}
setDir jeu _ _ = jeu

getHitbox :: Jeu -> NumPlayer -> Hitbox
getHitbox (GameOver c) _ = Rect (Coord 0 0) 0 0
getHitbox j Player1 = hitboxc (joueur1 j)
getHitbox j Player2 = hitboxc (joueur2 j)


--Gestion Hitbox
--Invariant, une hitbox ne peut pas être vide
propInvHitbox :: Hitbox -> Bool
propInvHitbox Rect {} = True
propInvHitbox (Composite s) = not (null s)

--Constructeur de Hitbox (2.2)
smartMkHitbox :: Seq (Coord, Coord) -> Hitbox
smartMkHitbox s = Composite (fmap transf s) where
  transf (Coord x1 y1,Coord x2 y2) = do
    let topLeft = Coord (min x1 x2) (min y1 y2)
    let bottRight = Coord (max x1 x2) (max y1 y2)
    Rect topLeft (distX topLeft bottRight) (distY topLeft bottRight)
    
--Fonctions Utilitaires pour smartMkHitbox
distX :: Coord -> Coord -> Integer
distX (Coord x1 _) (Coord x2 _) = x2-x1

distY :: Coord -> Coord -> Integer
distY (Coord _ y1) (Coord _ y2) = y2-y1
--Fin Fonctions Utilitaires pour smartMkHitbox

--Vérification si une coordonée appartient à une Hitbox
appartient :: Coord -> Hitbox -> Bool
appartient (Coord x y) (Rect (Coord xr yr) larg long) = (x >= xr) && (x <= xr + larg) && (y >= yr) && (y <= yr + long)
appartient c (Composite s) = any (appartient c) s

--Bouge la Hitbox
bougeHitbox :: Hitbox -> Mouvement -> Hitbox
bougeHitbox (Rect c larg long) mv = Rect (bougeCoord c mv) larg long
bougeHitbox (Composite s) mv = Composite (fmap (flip bougeHitbox mv) s)

--Propriété qui doit toujours être vrai
propBougeHitbox :: Coord -> Hitbox -> Mouvement -> Bool
propBougeHitbox c h mv = appartient c h == appartient (bougeCoord c mv) (bougeHitbox h mv)

--Bouge la Hitbox de manière sûre
bougeHitboxSafe :: Hitbox -> Mouvement -> Zone -> Maybe Hitbox
bougeHitboxSafe h mv z =
  let newHitbox = bougeHitbox h mv
  in if verifHitbox newHitbox z then Just newHitbox else Nothing

--Fonction qui vérifie si la hitbox est toujours dans la zone    
verifHitbox :: Hitbox -> Zone -> Bool
verifHitbox (Rect (Coord xr yr) larg long) (Zone w h)
  | xr < 0 || xr+larg > w || yr < 0 || yr+long > h = False
  | otherwise = True
verifHitbox (Composite s) z = any (flip verifHitbox z) s

--Fonction qui renvoie vrai s'il y a collision entre les Hitbox
collision :: Hitbox -> Hitbox -> Bool
collision (Rect c1 larg1 long1) (Rect c2 larg2 long2) = getX c1 < getX c2 + larg2 &&
                                                        getX c1 + larg1 > getX c2 &&
                                                        getY c1 < getY c2 + long2 &&
                                                        getY c1 + long1 > getY c2
collision h1 (Composite s2) = any (collision h1) s2
collision (Composite s1) r2 = any (collision r2) s1


--Question 3.1 D´ecrire des invariants pour les types ci-dessus (au besoin, modifier ces types) permettant
--de garantir des propri´et´es coh´erentes, comme par exemple :
-- • les positions des deux combattants sont inclues dans la zone de jeu,
-- • les hitbox des deux combattants ne se chevauchent pas,
-- • les deux combattants se font face.

checkMaybe :: Maybe a -> Bool
checkMaybe (Just _) = True
checkMaybe Nothing = False

--Les combattants se situent dans la zone
propInZone :: Jeu -> Bool
propInZone (GameOver c)  = True
propInZone EnCours {joueur1 = c1, joueur2 = c2, zoneJeu = z} = checkMaybe (verifCoord (positionc c1) z) &&
                                                               checkMaybe (verifCoord (positionc c2) z)

--Aucune Hitbox ne doit être en collision
propHitboxHit :: Jeu -> Bool
propHitboxHit (GameOver c)  = True
propHitboxHit EnCours {joueur1 = c1, joueur2 = c2, zoneJeu = z} = verifHitbox (hitboxc c1) z &&
                                                               verifHitbox (hitboxc c2) z

--Vérification que les combattants sont face à face
propFaceToFace :: Jeu -> Bool
propFaceToFace (GameOver c)  = True
propFaceToFace EnCours {joueur1 = c1, joueur2 = c2} = verifFace (facec c1) (facec c2) where
  verifFace D G = True
  verifFace G D = True
  verifFace _ _ = False

--Vérification Vie des combattants
propVie :: Jeu -> Bool
propVie (GameOver c)  = True
propVie EnCours {joueur1 = c1, joueur2 = c2} = verifVie (etatx c1) (etatx c2) where
  verifVie Ko Ko = True
  verifVie (Ok nb1) Ko = nb1 > 0
  verifVie (Ok nb1) (Ok nb2) = nb1 > 0 && nb2 > 0
  verifVie Ko (Ok nb2) = nb2 > 0

--Question 3.2 flou ? Une propriété qui vérifie selon un condition qui se passe avant ? :/
--propVie est peut être suffisant

--Gestion du Mouvement
bougeJoueurCoord :: Combattant -> Mouvement -> Zone -> Combattant
bougeJoueurCoord c m z = check --Vérification de Maybe dans check
    (bougeCoordSafe (positionc c) m z) where
                              check Nothing = c
                              check (Just newPos) = updatePos c newPos

--Bouge la Hitbox en respectant les propriétés (Dans la Zone + Collision)
bougeJoueurHitbox :: Combattant -> Combattant -> Mouvement -> Zone -> (Combattant,Bool)
bougeJoueurHitbox c c2 m z = check --Vérification de Maybe dans check
    (bougeHitboxSafe (hitboxc c) m z) where
                              check Nothing = (c,False)
                              check (Just newHitbox) =
                                                    let newC = updateHitbox c newHitbox
                                                    in if collision (hitboxc c2) (hitboxc newC) then (c, False)
                                                    else (newC,True)

--bouge un joueur (Hitbox + Position) de manière safe
bougeJoueur :: NumPlayer -> Mouvement -> Jeu -> Jeu
bougeJoueur _ _ (GameOver c)  = GameOver c
bougeJoueur num m j = do
  let EnCours {joueur1 = c1, joueur2 = c2, zoneJeu = z} = j
  let player = case num of --Choix du joueur qui sera modifié
                Player1 -> (c1,c2)
                Player2 -> (c2,c1)
  let (c, b) = uncurry bougeJoueurHitbox player m z
  if b then (case num of --Choix du joueur qui sera modifié
                 Player1 -> j {joueur1 = bougeJoueurCoord c m z}
                 Player2 -> j {joueur2 = bougeJoueurCoord c m z})
        else j

propBougeJoueur :: Jeu -> Mouvement -> Bool
propBougeJoueur j m = do
  let j2 = bougeJoueur Player1 m j
  let j2' = bougeJoueur Player2 m j in
    propFaceToFace j && propHitboxHit j &&
    propFaceToFace j2 && propHitboxHit j2 &&
    propFaceToFace j2' && propHitboxHit j2'

propPreInitBougeJoueur :: Mouvement -> Bool
propPreInitBougeJoueur (Mouv d nb) = case d of
  D -> nb < 2 * fromIntegral S.screenW
  G -> nb < 2 * fromIntegral S.screenW
  H -> nb < fromIntegral S.screenH
  B -> nb < fromIntegral S.screenH

--Update la position d'un combattant
updatePos :: Combattant -> Coord -> Combattant
updatePos c p = c {positionc = p}

--Update la position d'une hitbox
updateHitbox :: Combattant -> Hitbox -> Combattant
updateHitbox c h = c {hitboxc = h}

getJoueur :: Jeu -> NumPlayer -> Combattant
getJoueur jeu@EnCours{joueur1 = j1} Player1 = j1
getJoueur jeu@EnCours{joueur2 = j2} Player2 = j2

getJoueurSafe :: Jeu -> NumPlayer -> Maybe Combattant
getJoueurSafe jeu@EnCours{joueur1 = j1} Player1 = Just j1
getJoueurSafe jeu@EnCours{joueur2 = j2} Player2 = Just j2
getJoueurSafe _ _ = Nothing

setJoueur :: Jeu -> NumPlayer -> Combattant -> Jeu
setJoueur jeu@EnCours{} Player1 newPlayer = jeu{joueur1=newPlayer}
setJoueur jeu@EnCours{} Player2 newPlayer = jeu{joueur2=newPlayer}
setJoueur jeu _ _ = jeu

actionJoueur :: Action -> NumPlayer -> Jeu -> Jeu 
actionJoueur new@(Action newAction newFrame) player jeu =
    let joueur = getJoueur jeu player in
    let cur@(Action curAction _) = (action joueur) in
        if newAction /= curAction && (isInterruptible cur) then
            setJoueur jeu player joueur{action=new}
        else 
            jeu

class Applicative f => Alternative f where
    -- | The identity of '<|>'
    empty :: f a
    -- | An associative binary operation
    (<|>) :: f a -> f a -> f a

instance Alternative Maybe where
    empty = Nothing
    Nothing <|> r = r
    l       <|> _ = l

--C'est un foncteur applicatif mais pour l'union
(<||>) :: Alternative f => (a -> a -> a) -> f a -> f a -> f a
(<||>) f a b = (f <$> a <*> b) <|> a <|> b

(<|.|>) = (<||>) (.)

fromMaybe     :: a -> Maybe a -> a
fromMaybe d x = case x of {Nothing -> d;Just v  -> v}

--Etape du jeu (Etape du Moteur Engine)
jeuStep :: RealFrac a => Jeu -> Keyboard -> a -> Jeu
jeuStep jstate kbd deltaTime = 
              --Touches clavier Joueur 1
  
  let modif = 
            (fromMaybe
                (actionJoueur (Action Idle 0) Player1)
                (
                    (if K.keypressed KeycodeLeft kbd
                    then Just (actionJoueur (Action WalkLeft 0) Player1) else Nothing)
                <|.|>
                    (if K.keypressed KeycodeRight kbd
                    then Just (actionJoueur (Action WalkRight 0) Player1) else Nothing)
               <|.|>
                    (if K.keypressed KeycodeUp kbd
                    then Just (actionJoueur (Action Jump 0)  Player1) else Nothing)
               <|.|>
                    (if K.keypressed KeycodeN kbd
                    then Just (actionJoueur (Action Block 0)  Player1) else Nothing)
               <|.|>
                    (if K.keypressed KeycodeB kbd
                    then Just (actionJoueur (Action Punch 0)  Player1) else Nothing)
               <|.|>
                    (if K.keypressed KeycodeV kbd
                    then Just (actionJoueur (Action Kick 0)  Player1) else Nothing)
               <|.|>
                    (if K.keypressed KeycodeDown kbd
                    then Just (actionJoueur (Action Crouch 0)  Player1) else Nothing)
                )
            )
          .

              --Touches clavier Joueur 2
              
             (fromMaybe 
                (actionJoueur (Action Idle 0) Player2)
                (
                  (if K.keypressed KeycodeQ kbd
                    then Just (actionJoueur (Action WalkLeft 0) Player2) else Nothing)
               <|.|>
                    (if K.keypressed KeycodeD kbd
                    then Just (actionJoueur (Action WalkRight 0) Player2) else Nothing)
               <|.|>
                    (if K.keypressed KeycodeZ kbd
                    then Just (actionJoueur (Action Jump 0)  Player2) else Nothing)
               <|.|>
                    (if K.keypressed KeycodeA kbd
                    then Just (actionJoueur (Action Block 0)  Player2) else Nothing)
               <|.|>
                    (if K.keypressed KeycodeE kbd
                    then Just (actionJoueur (Action Punch 0)  Player2) else Nothing)
               <|.|>
                    (if K.keypressed KeycodeR kbd
                    then Just (actionJoueur (Action Kick 0)  Player2) else Nothing)
               <|.|>
                    (if K.keypressed KeycodeS kbd
                    then Just (actionJoueur (Action Crouch 0)  Player2) else Nothing)
              ))

  in modif (stepJeuAction jstate)

stepActionJoueur :: Jeu -> NumPlayer -> Jeu
stepActionJoueur jeu@EnCours{joueur1 = j1} Player1 = jeu{joueur1=j1{action=(stepAction (action j1))}}
stepActionJoueur jeu@EnCours{joueur2 = j2} Player2 = jeu{joueur2=j2{action=(stepAction (action j2))}}
stepActionJoueur jeu _ = fromMaybe jeu (Just jeu)

stepJeuAction :: Jeu -> Jeu
stepJeuAction jeu =
    let defHitbox1 = getDefensiveHitboxActionPlayer jeu Player1 in
    let offHitbox1 = getOffensiveHitboxActionPlayer jeu Player1 in
    let defHitbox2 = getDefensiveHitboxActionPlayer jeu Player2 in
    let offHitbox2 = getOffensiveHitboxActionPlayer jeu Player2 in
    let jeu' = if collision offHitbox1 defHitbox2 then updateEffectActionPlayer jeu Player1 else jeu in
    let jeu'' = if collision offHitbox2 defHitbox1 then updateEffectActionPlayer jeu' Player2 else jeu' in
    let jeu''' = updateActionPlayer jeu'' Player1 in
    let jeu'''' = updateActionPlayer jeu''' Player2 in
    stepActionJoueur (stepActionJoueur jeu'''' Player1) Player2


--------------------- ACTION ---------------------------

data ActionName =
    Idle
    | Crouch
    | Block
    | WalkLeft
    | WalkRight
    | Jump
    | Kick
    | Punch
    | KoAction
    deriving(Show, Eq)

data Action = Action ActionName Integer deriving(Show)

getActionName :: Action -> ActionName
getActionName (Action name _) = name



updatePlayerWalkLeft :: Jeu -> NumPlayer -> Jeu
updatePlayerWalkLeft jeu joueur = bougeJoueur joueur (Mouv G (fromIntegral D.sizeStep)) jeu

updatePlayerWalkRight :: Jeu -> NumPlayer-> Jeu
updatePlayerWalkRight jeu joueur = bougeJoueur joueur (Mouv D (fromIntegral D.sizeStep)) jeu

updatePlayerJump :: Jeu -> NumPlayer -> Jeu
updatePlayerJump jeu joueur
    | (getFrame jeu joueur) < (D.jumpSpriteDuration * D.jumpFrameNumber) `div` 2 = bougeJoueur joueur (Mouv  H D.jumpSpeed) jeu
    | otherwise = bougeJoueur joueur (Mouv  B D.jumpSpeed) jeu

updateIdentity :: Jeu -> NumPlayer -> Jeu
updateIdentity jeu _ = jeu


updateActionPlayer :: Jeu -> NumPlayer -> Jeu
updateActionPlayer jeu joueur = updateActionPlayerDispatch (getAction jeu joueur) jeu joueur

updateActionPlayerDispatch :: Action -> (Jeu -> NumPlayer -> Jeu)
updateActionPlayerDispatch (Action WalkLeft _) = updatePlayerWalkLeft
updateActionPlayerDispatch (Action WalkRight _) = updatePlayerWalkRight
updateActionPlayerDispatch (Action Jump _) = updatePlayerJump
updateActionPlayerDispatch (Action _ _) = updateIdentity

rotateHitbox :: Hitbox -> Hitbox
rotateHitbox (Rect (Coord x y) largeur hauteur) = Rect (Coord (x - largeur) y) largeur hauteur
rotateHitbox (Composite hitboxes) = Composite (fmap rotateHitbox hitboxes)

rotateHitboxIfNeeded :: Jeu -> NumPlayer -> Hitbox -> Hitbox
rotateHitboxIfNeeded jeu player hitbox
    | getDir jeu player == G = rotateHitbox hitbox
    | otherwise = hitbox

adaptHitbox :: Jeu -> NumPlayer -> Hitbox -> Hitbox
adaptHitbox jeu player hitbox = rotateHitboxIfNeeded jeu player (bougeHitbox (bougeHitbox hitbox (Mouv D (getX (getCoord jeu player)))) (Mouv B (getY (getCoord jeu player))))

getHitboxIdentity :: Jeu -> NumPlayer -> Hitbox
getHitboxIdentity jeu player = getHitbox jeu player

getDefensiveHitboxCrouch :: Jeu -> NumPlayer -> Hitbox
getDefensiveHitboxCrouch jeu player = adaptHitbox jeu player crouchHitbox


getDefensiveHitboxActionPlayer :: Jeu -> NumPlayer -> Hitbox
getDefensiveHitboxActionPlayer jeu joueur = getDefensiveHitboxActionPlayerDispatch (getAction jeu joueur) jeu joueur

getDefensiveHitboxActionPlayerDispatch :: Action -> (Jeu -> NumPlayer -> Hitbox)
getDefensiveHitboxActionPlayerDispatch (Action Crouch _) = getDefensiveHitboxCrouch
getDefensiveHitboxActionPlayerDispatch (Action _ _) = getHitboxIdentity



kickHitbox :: Hitbox
kickHitbox = Rect (Coord 20 (-25)) 50 50

punchHitbox :: Hitbox
punchHitbox = Rect (Coord 20 (-30)) 50 25

crouchHitbox :: Hitbox
crouchHitbox = Rect (Coord 0 40) 30 40

getOffensiveHitboxKick :: Jeu -> NumPlayer -> Hitbox
getOffensiveHitboxKick jeu player = adaptHitbox jeu player kickHitbox

getOffensiveHitboxPunch :: Jeu -> NumPlayer -> Hitbox
getOffensiveHitboxPunch jeu player = adaptHitbox jeu player punchHitbox

noOffensiveHitbox :: Jeu -> NumPlayer -> Hitbox
noOffensiveHitbox jeu player = Rect (Coord 0 0) 0 0

getOffensiveHitboxActionPlayer :: Jeu -> NumPlayer -> Hitbox
getOffensiveHitboxActionPlayer jeu joueur = getOffensiveHitboxActionPlayerDispatch (getAction jeu joueur) jeu joueur

getOffensiveHitboxActionPlayerDispatch :: Action -> (Jeu -> NumPlayer -> Hitbox)
getOffensiveHitboxActionPlayerDispatch (Action Kick _) = getOffensiveHitboxKick
getOffensiveHitboxActionPlayerDispatch (Action Punch _) = getOffensiveHitboxPunch
getOffensiveHitboxActionPlayerDispatch (Action _ _) = noOffensiveHitbox



otherPlayer :: NumPlayer -> NumPlayer
otherPlayer Player1 = Player2

damageEtat :: EtatCombattant -> Integer -> EtatCombattant
damageEtat (Ok hp) damage
    | hp > damage = (Ok (hp - damage))
    | otherwise = Ko
damageEtat Ko _ = Ko

damageCombattant :: Combattant -> Integer -> Combattant
damageCombattant joueur@Comb {etatx = ex} damage = joueur {etatx = (damageEtat ex damage)}


damagePlayer :: Jeu -> NumPlayer -> Integer -> Jeu
damagePlayer jeu@EnCours {joueur1 = j1} Player1 damage = jeu {joueur1 = (damageCombattant j1 damage)}
damagePlayer jeu@EnCours {joueur2 = j2} Player2 damage = jeu {joueur2 = (damageCombattant j2 damage)}


effectPunch :: Jeu -> NumPlayer -> Jeu
effectPunch jeu player =
  case getActionName (getAction jeu (otherPlayer player)) of
        Block -> jeu
        _ -> damagePlayer jeu (otherPlayer player) D.punchDamage


effectKick :: Jeu -> NumPlayer -> Jeu
effectKick jeu player =
    case getActionName (getAction jeu (otherPlayer player)) of
        Block -> jeu
        _ -> damagePlayer jeu (otherPlayer player) D.kickDamage


noEffect :: Jeu -> NumPlayer -> Jeu
noEffect jeu player = jeu


updateEffectActionPlayer :: Jeu -> NumPlayer -> Jeu
updateEffectActionPlayer jeu joueur = updateEffectActionPlayerDispatch (getAction jeu joueur) jeu joueur


updateEffectActionPlayerDispatch :: Action -> (Jeu -> NumPlayer -> Jeu)
updateEffectActionPlayerDispatch (Action Kick _) = effectKick
updateEffectActionPlayerDispatch (Action Punch _) = effectPunch
updateEffectActionPlayerDispatch (Action _ _) = noEffect


getIdleSprite :: Integer -> Integer
getIdleSprite frame = D.idleSpriteBegin


getCrouchSprite :: Integer -> Integer
getCrouchSprite frame = D.crouchSpriteBegin


getBlockSprite :: Integer -> Integer
getBlockSprite frame = D.blockSpriteBegin


getWalkLeftSprite :: Integer -> Integer
getWalkLeftSprite frame = D.walkSpriteBegin + walkFrameNumber - 1 - frame `div` D.walkSpriteDuration


getWalkRightSprite :: Integer -> Integer
getWalkRightSprite frame = D.walkSpriteBegin + frame `div` D.walkSpriteDuration


getJumpSprite :: Integer -> Integer
getJumpSprite frame = D.jumpSpriteBegin + frame `div` D.jumpSpriteDuration


getPunchSprite :: Integer -> Integer
getPunchSprite frame = D.punchSpriteBegin + frame `div` D.punchSpriteDuration


getKickSprite :: Integer -> Integer
getKickSprite frame = D.kickSpriteBegin + frame `div` D.kickSpriteDuration


getKoSprite :: Integer -> Integer
getKoSprite frame = D.koSpriteBegin + frame `div` D.koSpriteDuration

getSpriteActionPlayer :: Jeu -> NumPlayer -> Integer
getSpriteActionPlayer jeu joueur = getSpriteActionPlayerDispatch (getAction jeu joueur)


getSpriteActionPlayerDispatch :: Action -> Integer
getSpriteActionPlayerDispatch (Action Idle frame) = getIdleSprite frame
getSpriteActionPlayerDispatch (Action Crouch frame) = getCrouchSprite frame
getSpriteActionPlayerDispatch (Action Block frame) = getBlockSprite frame
getSpriteActionPlayerDispatch (Action WalkLeft frame) = getWalkLeftSprite frame
getSpriteActionPlayerDispatch (Action WalkRight frame) = getWalkRightSprite frame
getSpriteActionPlayerDispatch (Action Jump frame) = getJumpSprite frame
getSpriteActionPlayerDispatch (Action Punch frame) = getPunchSprite frame
getSpriteActionPlayerDispatch (Action Kick frame) = getKickSprite frame
getSpriteActionPlayerDispatch (Action KoAction frame) = getKoSprite frame


isInterruptible :: Action -> Bool
isInterruptible (Action Idle _) = True
isInterruptible (Action Crouch _) = True
isInterruptible (Action Block _) = True
isInterruptible (Action WalkLeft _) = True
isInterruptible (Action WalkRight _) = True
isInterruptible (Action Jump _) = False
isInterruptible (Action Punch _) = False
isInterruptible (Action Kick _) = False
isInterruptible (Action KoAction _) = False

stepAction :: Action -> Action
stepAction (Action Idle _) = (Action Idle 0)
stepAction (Action Crouch _) = (Action Crouch 0)
stepAction (Action Block _) = (Action Block 0)

stepAction (Action WalkLeft i)
    | i + 1 < D.walkSpriteDuration * D.walkFrameNumber = (Action WalkLeft (i + 1))
    | otherwise = (Action WalkLeft 0)

stepAction (Action WalkRight i)
    | i + 1 < D.walkSpriteDuration * D.walkFrameNumber = (Action WalkRight (i + 1))
    | otherwise = (Action WalkRight 0)

stepAction (Action Jump i)
    | i + 1 < D.jumpSpriteDuration * D.jumpFrameNumber = (Action Jump (i + 1))
    | otherwise = (Action Idle 0)

stepAction (Action Punch i)
    | i + 1 < D.punchSpriteDuration * D.punchFrameNumber = (Action Punch (i + 1))
    | otherwise = (Action Idle 0)

stepAction (Action Kick i)
    | i + 1 < D.kickSpriteDuration * D.kickFrameNumber = (Action Kick (i + 1))
    | otherwise = (Action Idle 0)

stepAction (Action KoAction i)
    | i + 1 < D.koSpriteDuration * D.koFrameNumber = (Action KoAction (i + 1))
    | otherwise = (Action KoAction i)




