{-# LANGUAGE OverloadedStrings #-}
module Main where

import Control.Monad (unless)
import Control.Concurrent (threadDelay)

import Data.Set (Set)
import qualified Data.Set as Set

import Data.List (foldl')


import Foreign.C.Types (CInt (..) )

import SDL
import SDL.Time (time, delay)
import Linear (V4(..))

import TextureMap (TextureMap, TextureId (..))
import qualified TextureMap as TM

import Sprite (Sprite)
import qualified Sprite as S

import SpriteMap (SpriteMap, SpriteId (..))
import qualified SpriteMap as SM

import Keyboard (Keyboard)
import qualified Keyboard as K

import qualified Debug.Trace as T

import Model (Jeu)
import qualified Model as M

import qualified HardCodedParameters as H

import Screen

getDataFileName :: FilePath -> IO FilePath
getDataFileName = return

screenWidth, screenHeight :: CInt
(screenWidth, screenHeight) = (640, 480)

data RenderPos = MyCentered | At (Point V2 CInt)

loadTexture :: SDL.Renderer -> FilePath -> IO SDL.Texture
loadTexture renderer path = do
  bmp <- SDL.loadBMP path
  SDL.createTextureFromSurface renderer bmp <* SDL.freeSurface bmp


renderTexture :: Renderer -> Texture -> RenderPos -> IO ()
renderTexture renderer tex pos = do
  ti <- queryTexture tex
  let (w, h) = (textureWidth ti, textureHeight ti)
      pos'   = case pos of
        At p     -> p
        MyCentered -> let cntr a b = (a - b) `div` 2
                    in P $ V2 (cntr screenWidth w) (cntr screenHeight h)
      extent = V2 w h
  copy renderer tex Nothing (Just $ Rectangle pos' extent)

  

loadBackground :: Renderer-> FilePath -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
loadBackground rdr path tmap smap = do
  tmap' <- TM.loadTexture rdr path (TextureId "background") tmap
  let sprite = S.defaultScale $ S.addImage S.createEmptySprite $ S.createImage (TextureId "background") (S.mkArea 0 0 640 480)
  let smap' = SM.addSprite (SpriteId "background") sprite smap
  return (tmap', smap')

loadPerso :: String -> Renderer-> FilePath -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
loadPerso id rdr path tmap smap = do
  tmap' <- TM.loadTexture rdr path (TextureId id) tmap
  let sprite = S.defaultScale $ S.addImage S.createEmptySprite $ S.createImage (TextureId id) (S.mkArea 0 0 130 106)
  let smap' = SM.addSprite (SpriteId id) sprite smap
  return (tmap', smap')


addSprite :: String -> String -> Renderer -> FilePath -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
addSprite id newid rdr path tmap smap = do
  tmap2 <- TM.loadTexture rdr path (TextureId newid) tmap
  return (tmap2,  SM.changeSprite (SpriteId id) (S.addImage (SM.fetchSprite (SpriteId id) smap) $ S.createImage (TextureId newid) (S.mkArea 0 0 130 106)) smap)

main :: IO ()
main = do
  initializeAll
  window <- createWindow "Minijeu" $ defaultWindow { windowInitialSize = V2 (fromIntegral Screen.screenW) (fromIntegral Screen.screenH) }
  renderer <- createRenderer window (-1) defaultRenderer
  -- chargement de l'image du fond
  (tmap, smap) <- loadBackground renderer "assets/background.bmp" TM.createTextureMap SM.createSpriteMap
  -- chargement du personnage
  (tmap', smap') <- loadPerso  "perso1" renderer "assets/ry2/walk/walk1.bmp" tmap smap

  (tmap2', smap2') <- loadPerso "perso2" renderer"assets/ry/walk/walk1.bmp" tmap' smap'

  (tmap5,smap5) <- return (tmap2',smap2') >>= uncurry (addSprite "perso2" "perso2walk2" renderer "assets/ry/walk/walk2.bmp") >>= uncurry (addSprite "perso2" "perso2walk3" renderer "assets/ry/walk/walk3.bmp") >>= (uncurry (addSprite "perso2" "perso2jump1" renderer "assets/ry/jump/jump1.bmp")) >>= uncurry (addSprite "perso2" "perso2jump2" renderer "assets/ry/jump/jump2.bmp") >>= uncurry (addSprite "perso2" "perso2jump3" renderer "assets/ry/jump/jump3.bmp") >>= uncurry (addSprite "perso2" "perso2kick1" renderer "assets/ry/Fkick/Fkick1.bmp") >>= uncurry (addSprite "perso2" "perso2kick2" renderer "assets/ry/Fkick/Fkick2.bmp") >>= uncurry (addSprite "perso2" "perso2couchBlock3" renderer "assets/ry/block/couchBlock3.bmp") >>= uncurry (addSprite "perso2" "perso2couchBlock4" renderer "assets/ry/block/couchBlock4.bmp") >>= uncurry (addSprite "perso2" "perso2punch1" renderer "assets/ry/punch/punch1.bmp") >>= uncurry (addSprite "perso2" "perso2punch2" renderer "assets/ry/punch/punch2.bmp") >>= uncurry (addSprite "perso2" "perso2punch3" renderer "assets/ry/punch/punch3.bmp") >>= uncurry (addSprite "perso2" "perso2ko1" renderer "assets/ry/ko/ko1.bmp") >>= uncurry (addSprite "perso2" "perso2ko2" renderer "assets/ry/ko/ko2.bmp") >>= uncurry (addSprite "perso2" "perso2ko3" renderer "assets/ry/ko/ko3.bmp")
  
  (tmap6,smap6) <- return (tmap5,smap5) >>= uncurry (addSprite "perso1" "perso1walk2" renderer "assets/ry2/walk/walk2.bmp") >>= uncurry (addSprite "perso1" "perso1walk3" renderer "assets/ry2/walk/walk3.bmp") >>= (uncurry (addSprite "perso1" "perso1jump1" renderer "assets/ry2/jump/jump1.bmp")) >>= uncurry (addSprite "perso1" "perso1jump2" renderer "assets/ry2/jump/jump2.bmp") >>= uncurry (addSprite "perso1" "perso1jump3" renderer "assets/ry2/jump/jump3.bmp") >>= uncurry (addSprite "perso1" "perso1kick1" renderer "assets/ry2/Fkick/Fkick1.bmp") >>= uncurry (addSprite "perso1" "perso1kick2" renderer "assets/ry2/Fkick/Fkick2.bmp") >>= uncurry (addSprite "perso1" "perso1couchBlock3" renderer "assets/ry2/block/couchBlock3.bmp") >>= uncurry (addSprite "perso1" "perso1couchBlock4" renderer "assets/ry2/block/couchBlock4.bmp") >>= uncurry (addSprite "perso1" "perso1punch1" renderer "assets/ry2/punch/punch1.bmp") >>= uncurry (addSprite "perso1" "perso1punch2" renderer "assets/ry2/punch/punch2.bmp") >>= uncurry (addSprite "perso1" "perso1punch3" renderer "assets/ry2/punch/punch3.bmp") >>= uncurry (addSprite "perso1" "perso1ko1" renderer "assets/ry2/ko/ko1.bmp") >>= uncurry (addSprite "perso1" "perso1ko2" renderer "assets/ry2/ko/ko2.bmp") >>= uncurry (addSprite "perso1" "perso1ko3" renderer "assets/ry2/ko/ko3.bmp")
  
  -- >>= uncurry (addSprite "perso2" "perso2jump2" renderer "assets/ry/walk/jump2.bmp")
  -- initialistation du jeu
  let jeuState = M.initJeu
  -- initialisation de l'état du clavier
  let kbd = K.createKeyboard
  -- lancement de la gameLoop
  gameLoop 60 renderer tmap6 smap6 kbd jeuState


--queryTexture : Fonction qui découpe une texture selon la taille et un point de reference
--int SDL_QueryTexture(SDL_Texture * texture,
--                     Uint32 * format, int *access,
--                     int *w, int *h);

--https://github.com/haskell-game/sdl2/blob/master/examples/twinklebear/Lesson02.hs

--https://hackage.haskell.org/package/sdl2-1.1.1/docs/Graphics-UI-SDL-Video.html

gameLoop :: (RealFrac a, Show a) => a -> Renderer -> TextureMap -> SpriteMap -> Keyboard -> Jeu -> IO ()
gameLoop frameRate renderer tmap smap kbd jeuState = do
  startTime <- time
  (SDL.P (SDL.V2 x y)) <- SDL.getAbsoluteMouseLocation
  -- putStrLn $ "x: " <> (show x) <> " y: " <> (show y)
  
  events <- pollEvents 
  let kbd' = K.handleEvents events kbd
  clear renderer

  let coordJ1 = M.getCoordJ1 jeuState
  
  let coordJ2 =  M.getCoordJ2 jeuState


  --- display background
  

  -- 5 Walk, 3 jump, 2 kick, 2 block, 3 punch, 3 ko

  S.displaySprite renderer tmap (SM.fetchSprite (SpriteId "background") smap)

  --- display combattant 1

  S.displaySprite renderer tmap (S.moveTo (S.changeImage (SM.fetchSprite (SpriteId "perso1") smap) (fromInteger (M.getSpriteActionPlayer jeuState M.Player1)) ) --Gestion changement image avec changeImage
                                 (fromIntegral ((M.getX coordJ1) -50))
                                 (fromIntegral (M.getY coordJ1)))
  -- display combattant 2

  S.displaySprite renderer tmap (S.moveTo (S.changeImage (SM.fetchSprite (SpriteId "perso2") smap) (fromInteger (M.getSpriteActionPlayer jeuState M.Player2)) ) --Gestion changement image avec changeImage
                                 (fromIntegral ((M.getX coordJ2) - 50))
                                 (fromIntegral (M.getY coordJ2)))
  ---
  present renderer
  endTime <- time
  let refreshTime = endTime - startTime
  let delayTime = floor (((1.0 / frameRate) - refreshTime) * 1000)
  threadDelay $ delayTime * 1000 -- microseconds
  endTime <- time
  let deltaTime = endTime - startTime
  -- putStrLn $ "Delta time: " <> (show (deltaTime * 1000)) <> " (ms)"
  putStrLn $ "Etats joueurs : " <> (show (M.getEtatsCombattants jeuState))
  --- update du game state
  let jeuState' = M.jeuStep jeuState kbd' deltaTime
  
  
  ---
  unless (K.keypressed KeycodeEscape kbd') (gameLoop frameRate renderer tmap smap kbd' jeuState')